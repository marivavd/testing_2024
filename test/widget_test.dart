// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:untitled1/domain/check.dart';

import 'package:untitled1/main.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/sign_in.dart';

void main() {
  Check check = Check();
  group('Tests', () {
    test('Валидация email', () => {
      expect(check.checkEmail('mar0_@gmail.com'), 0),
     
    });
    test('Валидация paswword', () => {
      expect(check.checkPassword(''), 0),
      expect(check.checkPassword('1223'), 1)
    });
    // testWidgets('', (widgetTester)async {
    //   FlutterLocalization localization = FlutterLocalization.instance;
    //   localization.init(mapLocales: [
    //     MapLocale('ru', AppLocale.RU),
    //     MapLocale('en', AppLocale.EN),
    //   ], initLanguageCode: 'ru');
    //   await widgetTester.pumpWidget(
    //       ScreenUtilInit(
    //         designSize: Size(375, 812),
    //         builder: (_, c){
    //           return MaterialApp(
    //               localizationsDelegates: localization.localizationsDelegates,
    //               supportedLocales: localization.supportedLocales,
    //               title: 'Flutter Demo',
    //               theme: ThemeData(
    //
    //                 colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
    //                 useMaterial3: true,
    //               ),
    //               home: SignIn()
    //           );
    //         },
    //
    //       )
    //   );
    //   await widgetTester.pumpAndSettle();
    //   await widgetTester.enterText(find.byKey(Key('Email')), 'mariba');
    //
    // });
  });
}
