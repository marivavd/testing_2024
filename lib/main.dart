import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/home.dart';
import 'package:untitled1/presentation/pages/sign_in.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: 'https://muozgansistgyudgofqc.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im11b3pnYW5zaXN0Z3l1ZGdvZnFjIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTQ5ODgzNDcsImV4cCI6MjAzMDU2NDM0N30.7RdYIRhMPaOtQj4kcd1JYuhMQ-hrSgnlkXkA5TEZgk4',
  );

  runApp(MyApp());
}
final supabase = Supabase.instance.client;
class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  FlutterLocalization localization = FlutterLocalization.instance;
  Connectivity connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> results;


  @override
  void initState() {
   WidgetsBinding.instance.addObserver(this);
    localization.init(mapLocales: [
      MapLocale('ru', AppLocale.RU),
      MapLocale('en', AppLocale.EN),
    ], initLanguageCode: (Platform.localeName.split("_").first != 'ru')?'en':'ru');
    localization.onTranslatedLanguage = (_) => setState(() {
      
    });

    super.initState();

    results = connectivity.onConnectivityChanged.listen((event) {
     if (event == ConnectivityResult.none){
       showError(context, 'Internet');
     }
    });

  }


  @override
  void didChangeLocales(List<Locale>? locales) {
    // TODO: implement didChangeLocales
    if (locales == null){
      return;
    }
    localization.translate((locales != 'ru')?'en':'ru');
    super.didChangeLocales(locales);
  }
  
  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(375, 812),
      builder: (_, c){
        return MaterialApp(
          localizationsDelegates: localization.localizationsDelegates,
          supportedLocales: localization.supportedLocales,
          title: 'Flutter Demo',
          theme: ThemeData(

            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          home: SignIn()
        );
      },

    );


  }
}


