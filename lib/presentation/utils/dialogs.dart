import 'package:flutter/material.dart';
import 'package:untitled1/domain/new_pass_use_case.dart';
import 'package:untitled1/presentation/pages/home.dart';
import 'package:untitled1/presentation/pages/sign_in.dart';

Future<void> showError(BuildContext context, String error)async{
  await showDialog(context: context, builder: (_){
    return AlertDialog(
      title: Text("error"),
      content: Text(error),
      actions: [
        TextButton(onPressed: (){
          Navigator.of(context).pop();
        }, child:Text(
          "OK"
        ) )
      ],
    );
  });
}


Future<void> showPassDialog(BuildContext context)async{
  var controller = TextEditingController();
  NewPassUseCase useCase = NewPassUseCase();
  await showDialog(context: context, builder: (_){
    return AlertDialog(
      title: Text("Enter text"),
      content: TextField(
        controller: controller,
      ),
      actions: [
        TextButton(onPressed: ()async{
          showLoading(context);
          useCase.pressButton(controller.text.split('').reversed.join(),
                  (p0){
            hideLoading(context);
            hideLoading(context);
            showNewPass(context, controller.text.split('').reversed.join(), );
                  },
                  (error)async{
            hideLoading(context);

            showError(context, error);
                  });
        }, child:Text(
            "OK"
        ) )
      ],
    );
  });
}


Future<void> showNewPass(BuildContext context, String password)async{
  var controller = TextEditingController();
  await showDialog(context: context, builder: (_){
    return AlertDialog(
      title: Text("Your new password"),
      content: SelectableText(password),
      actions: [
        TextButton(onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (_) => SignIn()));
        }, child:Text(
            "OK"
        ) )
      ],
    );
  });
}


void hideLoading(BuildContext context){
  Navigator.of(context).pop();
}

void showLoading(BuildContext context){
  showDialog(context: context, barrierDismissible: false,
      builder: (_){
    return PopScope(
      canPop: false,
        child: Dialog(
          surfaceTintColor: Colors.transparent,
          backgroundColor: Colors.transparent,
          child: Center(child: CircularProgressIndicator(),),
        ));
  });
}