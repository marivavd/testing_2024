import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';

mixin AppLocale{
  static const String test = 'test';
  static const String signInTitle = 'signInTitle';
  static const String signInText = 'signInText';
  static const String email = 'email';
  static const String password = 'password';
  static const String signInButton = 'signInButton';
  static const String recovery = 'recovery';
  static const String areYouFirst = 'areYouFirst';
  static const String create = 'create';
  static const String main = 'main';
  static const String categories = 'categories';
  static const String search = 'search';
  static const String all = 'all';
  static const String popular = 'popular';
  static const String action = 'actions';
  static const String addToCart = 'addToCart';
  static const String favourite = 'favourite';
  static const String cart = 'cart';
  static const String signUpTitle = 'signUpTitle';
  static const String signUpText = 'signUpText';
  static const String signUpButton = 'signUpButton';
  static const String name = 'name';
  static const String pdf = 'pdf';
  static const String haveAccount = 'haveAccount';
  static const String logIn = 'logIn';
  static const String forgotTitle = 'forgotTitle';
  static const String forgotText = 'forgotText';
  static const String forgotButton = 'forgotButton';
  static const String otpTitle = 'otpTitle';
  static const String otpText = 'otpText';
  static const String otpCode = 'otpCode';
  static const String changeAvatar = 'changeAvatar';









  static const Map<String, dynamic> RU = {
    test: 'hfudshufds',
    signInTitle: "Привет!",
    signInText: 'Заполните Свои Данные Или Продолжите Через Социальные Медиа',
    email:'Email',
    password: 'Пароль',
    signInButton: 'Войти',
    recovery: 'Востановить',
    areYouFirst: 'Вы впервые? ',
    create: 'Создать пользователя',
   main: 'Главная',
    categories: 'Категории',
    search: 'Поиск',
    all: 'Все',
    popular: 'Популярное',
    action: 'Акции',
    addToCart: 'В корзину',
    favourite: 'Избранное',
    cart: 'Корзина',
    pdf: 'Даю согласие на обработку\n персональных данных',
    signUpTitle: 'Регистрация',
    signUpText: 'Заполните Свои данные или продолжите через социальные медиа',
    signUpButton: 'Зарегистрироваться',
    name: 'Ваше имя',
    haveAccount: 'Есть аккаунт? ',
    logIn: 'Войти',
    forgotTitle:'Забыл пароль',
    forgotText: 'Введите свою учетную запись для сброса',
    forgotButton: 'Отправить',
    otpTitle: 'OTP проверка',
    otpText: 'Пожалуйста, проверьте свою электронную почту, чтобы увидеть код подтверждения',
    otpCode: 'OTP Код',
    changeAvatar: 'Изменить фото профиля'
  };


  static const Map<String, dynamic> EN = {
    test: 'fffffffff',
    signInTitle: "Hello Again!",
    signInText: 'Fill your details or continue with social media',
    email:'Email Address',
    password: 'Password',
    signInButton: 'Sign In',
    recovery: 'Recovery Password',
    areYouFirst: 'New User? ',
    create: 'Create Account',
    main: 'Explore',
    categories: 'Select Category',
    search: 'Looking for shoes',
    all: 'See all',
    popular: 'Popular Shoes',
    action: 'New Arrivals',
    addToCart: 'Add to Cart',
    favourite: 'Favourite',
    cart: 'Cart',
    pdf: 'I agree to the processing\nof personal data',
    signUpTitle: 'Register Account',
    signUpText: 'Fill your details or continue with social media',
    signUpButton: 'Sign up',
    name: 'Your Name',
    haveAccount: 'Already Have Account? ',
    logIn: 'Log In',
    forgotTitle:'Forgot Password',
    forgotText: 'Enter your Email account to reset your password',
    forgotButton: 'Reset password',
    otpTitle: 'OTP Verification',
    otpText: 'Please check your email to see the verification code',
    otpCode: 'OTP Code',
    changeAvatar: 'Change Profile Picture'
  };
}