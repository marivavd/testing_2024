import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/data/models/category.dart';
import 'package:untitled1/data/models/model_product.dart';
import 'package:untitled1/domain/categories_use_case.dart';
import 'package:untitled1/domain/products_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/popular.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/cardproduct.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<ModelCategory> categories = [];
  List<ModelProduct> products = [];
  bool flag = false;
  CatUseCase catUseCase = CatUseCase();
  ProductUseCase productUseCase = ProductUseCase();
  
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp)async { 
      await catUseCase.getAllCategories(
              (p0){
                categories = p0;
                categories.insert(0, ModelCategory(id: '0', title: AppLocale.all.getString(context)));

                setState(() {
                  
                });
              }, (error)async{
                showError(context, error);
      });
      await productUseCase.getAllBestSellerProducts(
              (p0){
            products = p0;
            flag = true;
            setState(() {

            });
          }, (error)async{
        showError(context, error);}
      );

    });
  }


  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 48.w,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(padding: EdgeInsets.only(top: 14.w),child: SvgPicture.asset('assets/Hamburger.svg'),),
                      Padding(padding: EdgeInsets.only(top: 3.w),
                      child: Row(
                        children: [
                          Transform.translate(offset: Offset(0, 3.w),
                            child: SvgPicture.asset('assets/Highlight_05.svg'),
                          ),
                          Text(
                            AppLocale.main.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    fontSize: 32.sp,
                                    height: 38/32.w,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFF2B2B2B)
                                )
                            ),
                          )
                        ],
                      )),
                      Stack(
                        children: [

                          Container(
                            height: 44.w,
                            width: 44.w,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(40.w)
                            ),
                            padding: EdgeInsets.all(10.w),
                            child: SvgPicture.asset('assets/bag-2.svg', height: 24.w, width: 24.w, color: Color(0xFF2B2B2B)),
                          ),
                          Padding(padding: EdgeInsets.only(top: 3.w, left: 34.w),child: SvgPicture.asset('assets/Ellipse 886.svg'),),
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 19.w,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        child: Container(
                          height: 52.w,
                          width: 269.w,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(14.w)
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 14.w),
                            child: Row(
                              children: [
                                SizedBox(width: 26.w,),
                                SvgPicture.asset('assets/search.svg'),
                                SizedBox(width: 12.w,),
                                Padding(padding: EdgeInsets.symmetric(vertical: 2.w),
                                child: Text(
                                  AppLocale.search.getString(context),
                                  style: GoogleFonts.raleway(
                                    textStyle: TextStyle(
                                      fontSize: 12.sp,
                                      height: 20/12.w,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xFF6A6A6A)
                                    )
                                  ),
                                ),),

                              ],
                            ),

                          ),
                        ),

                      ),
                      Container(
                        height: 52.w,
                        width: 52.w,
                        decoration: BoxDecoration(
                            color: Color(0xFF48B2E7),
                            borderRadius: BorderRadius.circular(40.w)
                        ),
                        padding: EdgeInsets.all(14.w),
                        child: SvgPicture.asset('assets/sliders.svg'),
                      )
                    ],
                  ),
                  SizedBox(height: 24.w,),
                  Text(
                    AppLocale.categories.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16.sp,
                            height: 19/16.w,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF2B2B2B)
                        )
                    ),
                  ),
                  SizedBox(height: 16.w,)
                ],
              ),
            ),
            SizedBox(height: 40.w,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
                itemBuilder: (_, index){
                  return Container(
                    height: 40.w,
                    width: 108.w,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.w)
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        categories[index].title
                      ),
                    ),
                  );
                },
                separatorBuilder: (_, index){
                  return SizedBox(width: 16.w,);
                },
                itemCount: categories.length),),
            SizedBox(height: 24.w,),
         Padding(padding: EdgeInsets.symmetric(
           horizontal: 20.w
         ),
         child: Column(
           crossAxisAlignment: CrossAxisAlignment.start,
           children: [
             Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: [
                 Text(
                   AppLocale.popular.getString(context),
                   style: GoogleFonts.raleway(
                       textStyle: TextStyle(
                           fontSize: 16.sp,
                           height: 19/16.w,
                           fontWeight: FontWeight.w600,
                           color: Color(0xFF2B2B2B)
                       )
                   ),
                 ),
                 InkWell(
                   child: Text(
                     AppLocale.all.getString(context),
                     style: GoogleFonts.raleway(
                         textStyle: TextStyle(
                             fontSize: 12.sp,
                             height: 16/12.w,
                             fontWeight: FontWeight.w500,
                             color: Color(0xFF48B2E7)
                         )
                     ),
                   ),
                   onTap: (){
                     Navigator.push(context, MaterialPageRoute(builder: (_) => Popular(products: products,)));
                   },
                 )
               ],
             ),
             SizedBox(height: 30.w,),
             Row(
               children: [
                 CardProduct(product: products[0]),
                 SizedBox(width: 15.w,),
                 CardProduct(product: products[1]),
               ],
             ),
             SizedBox(height: 29.w,),
             Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: [
                 Text(
                   AppLocale.action.getString(context),
                   style: GoogleFonts.raleway(
                       textStyle: TextStyle(
                           fontSize: 16.sp,
                           height: 19/16.w,
                           fontWeight: FontWeight.w600,
                           color: Color(0xFF2B2B2B)
                       )
                   ),
                 ),
                 InkWell(
                   child: Text(
                     AppLocale.all.getString(context),
                     style: GoogleFonts.raleway(
                         textStyle: TextStyle(
                             fontSize: 12.sp,
                             height: 16/12.w,
                             fontWeight: FontWeight.w500,
                             color: Color(0xFF48B2E7)
                         )
                     ),
                   ),
                   onTap: (){},
                 )
               ],
             ),
             SizedBox(height: 20.w,)
           ],

         ),)
          ],
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
