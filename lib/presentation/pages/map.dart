import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/domain/kandinsky_use_case.dart';

import 'package:yandex_mapkit/yandex_mapkit.dart';

import '../../data/models/model_ai.dart';
import '../../data/models/model_style.dart';
import '../utils/dialogs.dart';

class Kandinsky extends StatefulWidget {
  const Kandinsky({super.key});


  @override
  State<Kandinsky> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Kandinsky> {
  Uint8List? image;
  TextEditingController controller = TextEditingController();

  Map<String, double> rations = {
    "1 : 1": 1/1,
    "2 : 3": 2/3,
    "3 : 2": 3/2,
    "9 : 16": 9/16,
    "16 : 9" : 16/9
  };

  late MapEntry<String, double> ratio;
  ModelStyle? style;
  ModelAi? modelAI;

  List<ModelStyle> styles = [];
  bool isCensured = false;

  KandinskyUseCase useCase = KandinskyUseCase();

  @override
  void initState() {
    super.initState();
    ratio = rations.entries.first;
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      showLoading(context);

      await useCase.getStyles(
              (styles) {
            setState(() {
              this.styles = styles;
              style = this.styles.first;
            });
          },
              (error) {
            hideLoading(context);
            showError(context, error);
          }
      );

      await useCase.getModelAI(
              (model) {
            setState(() {
              modelAI = model;
              hideLoading(context);
            });
          },
              (error) {
            hideLoading(context);
            showError(context, error);
          }
      );
    });
  }


  Future<void> pressButtonGenerate() async {
    useCase.pressButton(
        ratio.value,
        controller.text,
        modelAI,
        style,
        onInit: (id){
          setState(() {
            isCensured = false;
            image = null;
          });
          showLoading(context);
        },
        onCheckStatus: (status) {

        },
        onDone: (image){
          setState(() {
            this.image = image;
          });
          hideLoading(context);
        },
        onCensured: (_) {
          setState(() {
            isCensured = true;
          });
          hideLoading(context);
        },
        onError: (error) {
          hideLoading(context);
          showError(context, error);
        }
    );
  }




  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            children: [
              (image != null)?Image.memory(
                image!, height: 300.w, width: 300.w,):Container(height: 300.w, width: 300.w,),
              SizedBox(height: 100.w,),
              TextField(
                controller: controller,
              ),
              SizedBox(height: 100.w,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50.w,
                  width: double.infinity,
                  child: OutlinedButton(
                      style: FilledButton.styleFrom(
                          backgroundColor: Color(0xFF48B2E7),
                          side: BorderSide(color: Colors.transparent),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14.w),
                              side: BorderSide(color: Color(0xFF48B2E7))
                          )
                      ),
                      onPressed: ()async{await pressButtonGenerate();},
                      child: Text('Generate',
                        style: GoogleFonts.raleway(
                            textStyle: TextStyle(
                                color: Color(0xFFF7F7F9),
                                fontSize: 14.sp,
                                height: 22/14.w,
                                fontWeight: FontWeight.w600
                            )
                        ),)),
                ),
              )
            ],
          ),
        ),
      )
    );

  }}
