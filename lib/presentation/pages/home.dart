import 'package:flutter/material.dart';
import 'package:untitled1/presentation/pages/cart.dart';
import 'package:untitled1/presentation/pages/favourite.dart';
import 'package:untitled1/presentation/pages/main_page.dart';
import 'package:untitled1/presentation/pages/map.dart';
import 'package:untitled1/presentation/pages/profile.dart';
import 'package:untitled1/presentation/widgets/bottom_navig.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int index = 0;
  List<Widget> sp = [MainPage(),Favourites(), Kandinsky(), Profile()];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Stack(
        children: [
          sp[index],
          Align(
            alignment: Alignment.bottomCenter,
            child: BottomNavigation(onSelect: (id){
              setState(() {
                index = id;
              });
            },
                onTapCart:  (){
                  Navigator.push(context, MaterialPageRoute(builder: (_) => Cart()));
                }
            ),
          )
        ],
      ),
    );
  }
}
