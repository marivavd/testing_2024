import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/data/models/model_product.dart';
import 'package:untitled1/domain/cart_use_case.dart';
import 'package:untitled1/domain/products_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/backButton.dart';

class Cart extends StatefulWidget {
  const Cart({super.key});

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  bool flag = false;
  List<ModelProduct> products = [];
  ProductUseCase useCase = ProductUseCase();
  CartUseCase cartUseCase = CartUseCase();
  int summa = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp)async {
      await useCase.getWholeProducts(
              (p0){
            List<ModelProduct> response = p0;
            for (var val in response){
              if (val.isBask){
                products.add(val);
                summa += int.parse(val.count.toString());
              }
            }
            flag = true;
            setState(() {

            });
          },
              (error)async{
            showError(context, error);
          });
    });
  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 48.w,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtontoBack(),
                  Padding(padding: EdgeInsets.only(top:12.w),child: Text(
                    AppLocale.cart.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16.sp,
                            height: 24/16.w,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF2B2B2B)
                        )
                    ),
                  ),),
                 Container(height: 44.w, width: 44.w,)
                ],
              ),
              SizedBox(height: 16.w,),
              Text(
                '${summa} товара'
              ),
              SizedBox(height: 13.w,),
            ],
          ),),
          Expanded(child: ListView.separated(
              itemBuilder: (_, index){
                return Padding(padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Container(
                  height: 104.w,
                  width: double.infinity,
                  child: Row(
                    children: [
                      Container(
                        height: 104.w,
                        color: Colors.white,
                        width: 58.w,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              child: Text("+"),
                              onTap: (){
                                products[index].count += 1;
                                summa += 1;
                                showLoading(context);
                                cartUseCase.updatecountBasket(
                                    products[index].id,
                                    products[index].count,
                                        (p0){
                                      hideLoading(context);
                                      setState(() {

                                      });
                                    }, (error)async{
                                  hideLoading(context);
                                  showError(context, error);
                                });
                              },
                            ),
                            Text(products[index].count.toString()),
                            InkWell(
                              child: Text("-"),
                              onTap: (){
                                if (products[index].count != 1){
                                  showLoading(context);
                                  products[index].count -= 1;
                                  summa -= 1;
                                  cartUseCase.updatecountBasket(
                                      products[index].id,
                                      products[index].count,
                                          (p0){
                                        hideLoading(context);
                                        setState(() {

                                        });
                                      }, (error)async{
                                    hideLoading(context);
                                    showError(context, error);
                                  });

                                }
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 104.w,
                        color: Colors.white,
                        width: 58.w,
                        child: Text(products[index].title),
                      ),
                      InkWell(
                        child: Container(
                          height: 104.w,
                          color: Colors.white,
                          width: 58.w,
                          child: Icon(Icons.delete)
                        ),
                        onTap: (){
                          showLoading(context);
                          cartUseCase.removeBasket(
                              products[index].id,
                                  (p0){
                                hideLoading(context);
                                products.remove(products[index]);
                                setState(() {

                                });
                              }, (error)async{
                            hideLoading(context);
                            showError(context, error);
                          });
                        },
                      )

                    ],
                  ),
                ),);
              },
              separatorBuilder:  (_, index){
                return SizedBox(height: 14.w,);
              },
              itemCount: products.length))
        ],
      )
    ):Center(child: CircularProgressIndicator());
  }
}
