import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/domain/products_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/favourites.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/backButton.dart';
import 'package:untitled1/presentation/widgets/cardproduct.dart';

import '../../data/models/model_product.dart';

class Favourites extends StatefulWidget {

  Favourites({super.key});

  @override
  State<Favourites> createState() => _FavouritesState();
}

class _FavouritesState extends State<Favourites> {
  List<ModelProduct> products = [];
  ProductUseCase useCase = ProductUseCase();
  bool flag = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp)async {
      await useCase.getWholeProducts(
              (p0){
                List<ModelProduct> response = p0;
                for (var val in response){
                  if (val.isFavourite){
                    products.add(val);
                  }
                }
                flag = true;
                setState(() {

                });
              },
              (error)async{
                showError(context, error);
              });
    });
  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 48.w,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ButtontoBack(),
                Padding(padding: EdgeInsets.only(top:12.w),child: Text(
                  AppLocale.favourite.getString(context),
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontSize: 16.sp,
                          height: 24/16.w,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF2B2B2B)
                      )
                  ),
                ),),
                InkWell(
                  onTap: (){
                  },
                  child: Container(
                      height: 40.w,
                      width: 40.w,
                      decoration: BoxDecoration(
                          color:Colors.white,
                          borderRadius: BorderRadius.circular(40.w)
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: SvgPicture.asset('assets/heart.svg', width: 18.w, height: 16.w),
                      )
                  ),
                )
              ],
            ),
            SizedBox(height: 20.w,),
           Expanded(child:  GridView.builder(
               gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                 mainAxisSpacing: 15.w,
                 crossAxisCount: 2,
                 crossAxisSpacing: 15.w,
                 childAspectRatio: 160/182.w,
               ),
               itemCount: products.length,
               itemBuilder:(_, inde){
                 return CardProduct(product: products[inde]);
               }))

          ],
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
