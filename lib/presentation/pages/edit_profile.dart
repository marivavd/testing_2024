import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:untitled1/domain/avatar_use_case.dart';
import 'package:untitled1/domain/profile_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/home.dart';
import 'package:untitled1/presentation/pages/profile.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/backButton.dart';
import 'package:untitled1/presentation/widgets/edit_profile_custom_field.dart';
import 'package:untitled1/presentation/widgets/profile_custom_field.dart';

class ProfileEdit extends StatefulWidget {
  const ProfileEdit({super.key});

  @override
  State<ProfileEdit> createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  bool redactor = false;
  var avatar;
  bool flag = false;
  ProfileUseCase useCase = ProfileUseCase();
  var firstname = TextEditingController();
  var lastname = TextEditingController();
  var address = TextEditingController();
  var phone = TextEditingController();
  String fullname = '';
  AvatarUseCase? avatarUseCase;
  void onPickAvatar(bytes){
    setState(() {
      avatar = bytes;
    });
  }

  void removeAvatar(){
    setState(() {
      avatar = null;
    });
  }

  Future<ImageSource?> chooseAvatar()async{
    ImageSource? source;
    await showDialog(context: context,
        builder: (_) => AlertDialog(
      title: Text('Choose source'),
      actions: [
        TextButton(onPressed: (){
          source = ImageSource.camera;
          Navigator.pop(context);
        }, child: const Text("Camera")),
        TextButton(onPressed: (){
          source = ImageSource.gallery;
          Navigator.pop(context);
        }, child: const Text("Gallery")),
        TextButton(onPressed: (){

        }, child: const Text("Kandinsky")),
      ],

    )
    );
    return source;

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await useCase.getAttributes(
              (p0){
            firstname.text = p0['firstname'];
            lastname.text = p0['lastname'];
            fullname = lastname.text + ' ' + firstname.text;
            address.text = p0['address'];
            phone.text = p0['phone'];
          },
              (error)async{
            showError(context, error);
          }
      );
      await useCase.getProfileAvatar(
              (p0) {
            avatar = p0;
            flag = true;
            setState(() {

            });
          },
              (error)async{
            showError(context, error);
          }
      );
    });
    avatarUseCase = AvatarUseCase(chooseImageSource: chooseAvatar, onPickAvatar: onPickAvatar, onRemoveAvatar: removeAvatar);
  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 60.w,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50.w,
                  width: 212.w,
                  child: FilledButton(
                    key: Key('SignInButton'),
                    onPressed: ()async{
                      showLoading(context);
                      useCase.edit(firstname.text,
                          lastname.text,
                          phone.text,
                          address.text,
                              (p0)async{
                        await useCase.loadAvatar(avatar,
                            (p1){
                          hideLoading(context);
                          Navigator.push(context, MaterialPageRoute(builder: (_) => Profile()));
                            },
                                (error)async{
                              hideLoading(context);
                              showError(context, error);
                            }
                        );
                              }, (error)async{
                        hideLoading(context);
                        showError(context, error);
                          }
                      );

                    },
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFF48B2E7),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14.w)
                        )
                    ),
                    child: Text(
                      'Сохранить',
                      textAlign: TextAlign.end,
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: Color(0xFFF7F7F9),
                              fontSize: 14.sp,
                              height: 22/14.w,
                              fontWeight: FontWeight.w600
                          )
                      ),
                    ),
                  ),
                ),
              ),

              SizedBox(height: 48.w,),
              Align(
                  alignment: Alignment.center,
                  child: ClipOval(
                    child: (avatar != null)?Container(
                      height: 96.w,
                      width: 96.w,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40.w)
                      ),
                      child: Image.memory(avatar, fit: BoxFit.cover,),
                    ):Image.asset('assets/Ellipse 22.png', height: 96.w, width: 96.w,),
                  )
              ),SizedBox(height: 8.w,),
              Align(alignment: Alignment.center,
                child: Text(
                  fullname,
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontSize: 20.sp,
                          fontWeight: FontWeight.w600,
                          height: 23/20.w,
                          color: Color(0xFF2B2B2B)
                      )
                  ),
                ),),
              SizedBox(height: 8.w,),
              InkWell(
                child: Align(alignment: Alignment.center,
                  child: Text(
                    AppLocale.changeAvatar.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w600,
                            height: 16/12.w,
                            color: Color(0xFF48B2E7)
                        )
                    ),
                  ),),
                onTap: (){
                  avatarUseCase?.pressButton((avatar == null)?false:true);
                },
              ),
              SizedBox(height: 20.w,),
              EditCustomField(label: 'Имя', controller: firstname,),
              SizedBox(height: 16.w,),
              EditCustomField(label: 'Фамилия', controller: lastname,),
              SizedBox(height: 16.w,),
              EditCustomField(label: 'Адрес', controller: address, ),
              SizedBox(height: 16.w,),
              EditCustomField(label: 'Телефон', controller: phone, ),
              SizedBox(height: 16.w,),


            ],
          ),
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
