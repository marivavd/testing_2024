import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/domain/forgot_use_case.dart';
import 'package:untitled1/domain/sifn_up_use_case.dart';
import 'package:untitled1/domain/sign_in_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/home.dart';
import 'package:untitled1/presentation/pages/otp.dart';
import 'package:untitled1/presentation/pages/sign_in.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/textfield.dart';

class Forgot extends StatefulWidget {
  const Forgot({super.key});

  @override
  State<Forgot> createState() => _ForgotState();
}

class _ForgotState extends State<Forgot> {
  var email = TextEditingController();
 ForgotUseCase forgotUseCase = ForgotUseCase();
  bool check =false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 66.w,),
              InkWell(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Container(
                    height: 44.w,
                    width: 44.w,
                    decoration: BoxDecoration(
                        color:Color(0xFFF7F7F9),
                        borderRadius: BorderRadius.circular(40.w)
                    ),
                    child: Padding(child: SvgPicture.asset('assets/back.svg', height: 5.5.w, width: 11.5.w,),
                      padding: EdgeInsets.symmetric(horizontal: 19.25.w, vertical: 16.25.w),)
                ),

              ),
              SizedBox(height: 11.w,),
              Center(
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    AppLocale.forgotTitle.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF2B2B2B),
                            fontSize: 32.sp,
                            height: 38/32.w,
                            fontWeight: FontWeight.w700
                        )
                    ),
                  ),

                ),
              ),
              SizedBox(height: 8.w,),
              Center(
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    AppLocale.forgotText.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF707B81),
                            fontSize: 16.sp,
                            height: 48/32.w,
                            fontWeight: FontWeight.w400
                        )
                    ),
                  ),

                ),
              ),
              SizedBox(height: 28.w,),
              CustomField(label: AppLocale.email.getString(context), hint: 'xyz@gmail.com', controller: email, key: Key('Email')),
             SizedBox(height: 40.w,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50.w,
                  width: double.infinity,
                  child: FilledButton(
                    key: Key('SignInButton'),
                    onPressed: ()async{
                      showLoading(context);
                      await forgotUseCase.pressButton(email.text,
                              (p0){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (_) => OTP(email: email.text)));
                          },
                              (error)async{
                            hideLoading(context);
                            await showError(context, error);
                          }
                      );
                    },
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFF48B2E7),
                        disabledBackgroundColor: Color(0xFF48B2E7),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14.w)
                        )
                    ),
                    child: Text(
                      AppLocale.forgotButton.getString(context),
                      textAlign: TextAlign.end,
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: Color(0xFFF7F7F9),
                              fontSize: 14.sp,
                              height: 22/14.w,
                              fontWeight: FontWeight.w600
                          )
                      ),
                    ),
                  ),
                ),
              ),



            ],
          ),
        ),
      ),
    );
  }
}
