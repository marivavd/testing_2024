import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/domain/sifn_up_use_case.dart';
import 'package:untitled1/domain/sign_in_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/home.dart';
import 'package:untitled1/presentation/pages/sign_in.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/textfield.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  var email = TextEditingController();
  var password = TextEditingController();
  var name = TextEditingController();
  SignUpUseCase useCase = SignUpUseCase();
  bool check =false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 66.w,),
              InkWell(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Container(
                    height: 44.w,
                    width: 44.w,
                    decoration: BoxDecoration(
                        color:Color(0xFFF7F7F9),
                        borderRadius: BorderRadius.circular(40.w)
                    ),
                    child: Padding(child: SvgPicture.asset('assets/back.svg', height: 5.5.w, width: 11.5.w,),
                      padding: EdgeInsets.symmetric(horizontal: 19.25.w, vertical: 16.25.w),)
                ),

              ),
              SizedBox(height: 11.w,),
              Center(
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    AppLocale.signUpTitle.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF2B2B2B),
                            fontSize: 32.sp,
                            height: 38/32.w,
                            fontWeight: FontWeight.w700
                        )
                    ),
                  ),

                ),
              ),
              SizedBox(height: 8.w,),
              Center(
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    AppLocale.signInText.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF707B81),
                            fontSize: 16.sp,
                            height: 48/32.w,
                            fontWeight: FontWeight.w400
                        )
                    ),
                  ),

                ),
              ),
              SizedBox(height: 30.w,),
              CustomField(label: AppLocale.name.getString(context), hint: 'xxxxxxxx', controller: name),
              SizedBox(height: 12.w,),
              CustomField(label: AppLocale.email.getString(context), hint: 'xyz@gmail.com', controller: email, key: Key('Email')),
              SizedBox(height: 12.w,),
              CustomField(label: AppLocale.password.getString(context), hint: '••••••••', controller: password, enableObscure: true,),
              SizedBox(height: 12.w,),
              Row(
                children: [
                  InkWell(
                    child: Container(
                      height: 18.w,
                      width: 18.w,
                      decoration: BoxDecoration(
                          color: (check)?Color(0xFF48B2E7):Color(0xFFF7F7F9),
                          borderRadius: BorderRadius.circular(6.w)
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: SvgPicture.asset('assets/Vector-3.svg'),
                      ),
                    ),
                    onTap: (){
                      setState(() {
                        check = !check;
                      });
                    },
                  ),
                  SizedBox(width: 10.w,),
                  InkWell(
                    child: Text(
                      AppLocale.pdf.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color(0xFF6A6A6A),
                              fontSize: 16.sp,
                              height: 38/32.w,
                              fontWeight: FontWeight.w500
                          )
                      ),

                    ),
                    onTap: (){
                    },
                  )
                ],
              ),
              SizedBox(height: 27.w,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50.w,
                  width: double.infinity,
                  child: FilledButton(
                    key: Key('SignInButton'),
                    onPressed: (check)?()async{
                      showLoading(context);
                      await useCase.pressButton(email.text,
                          password.text, name.text,
                              (p0){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (_) => Home()));
                          },
                              (error)async{
                            hideLoading(context);
                            await showError(context, error);
                          }
                      );
                    }:null,
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFF48B2E7),
                        disabledBackgroundColor: Color(0xFF48B2E7),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14.w)
                        )
                    ),
                    child: Text(
                      AppLocale.signUpButton.getString(context),
                      textAlign: TextAlign.end,
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: Color(0xFFF7F7F9),
                              fontSize: 14.sp,
                              height: 22/14.w,
                              fontWeight: FontWeight.w600
                          )
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 113.w,),
              InkWell(
                child: Center(
                  child: SizedBox(
                    width: double.infinity,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text: AppLocale.haveAccount.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  color: Color(0xFF6A6A6A),
                                  fontSize: 16.sp,
                                  height: 19/16.w,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                          children: [
                            TextSpan(
                                text: AppLocale.logIn.getString(context),
                                style: GoogleFonts.raleway(
                                    textStyle: TextStyle(
                                        color: Color(0xFF2B2B2B),
                                        fontSize: 16.sp,
                                        height: 19/16.w,
                                        fontWeight: FontWeight.w500
                                    )
                                ))
                          ]
                      ),
                    ),
                  ),
                ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (_) => SignIn()));
                },
              )


            ],
          ),
        ),
      ),
    );
  }
}
