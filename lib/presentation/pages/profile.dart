import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/domain/profile_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/edit_profile.dart';
import 'package:untitled1/presentation/pages/home.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/backButton.dart';
import 'package:untitled1/presentation/widgets/profile_custom_field.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool redactor = false;
  var avatar;
  bool flag = false;
  ProfileUseCase useCase = ProfileUseCase();
  var firstname = TextEditingController();
  var lastname = TextEditingController();
  var address = TextEditingController();
  var phone = TextEditingController();
  String fullname = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await useCase.getAttributes(
              (p0){
                firstname.text = p0['firstname'];
                lastname.text = p0['lastname'];
                fullname = lastname.text + firstname.text;
                address.text = p0['address'];
                phone.text = p0['phone'];
              },
          (error)async{
                showError(context, error);
          }
      );
      await useCase.getProfileAvatar(
              (p0) {
                avatar = p0;
                flag = true;
                setState(() {

                });
              },
              (error)async{
            showError(context, error);
          }
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 60.w,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset('assets/Hamburger.svg'),
                  Padding(padding: EdgeInsets.only(top:2.w),child: Text(
                    AppLocale.cart.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16.sp,
                            height: 20/16.w,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF2B2B2B)
                        )
                    ),
                  ),),
                  InkWell(
                    onTap: (){
                      setState(() {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => ProfileEdit()));
                      });
                    },
                    child: Container(
                        height: 25.w,
                        width: 25.w,
                        decoration: BoxDecoration(
                            color:Color(0xFF48B2E7),
                            borderRadius: BorderRadius.circular(40.w)
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: SvgPicture.asset('assets/pen.svg', width: 8.33.w, height: 7.81.w),
                        )
                    ),
                  )
                ],
              ),
              SizedBox(height: 48.w,),
              Align(
                alignment: Alignment.center,
                child: ClipOval(
                  child: (avatar != null)?Container(
                    height: 96.w,
                    width: 96.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40.w)
                    ),
                    child: Image.memory(avatar, fit: BoxFit.cover,),
                  ):Image.asset('assets/Ellipse 22.png', height: 96.w, width: 96.w,),
                )
              ),SizedBox(height: 8.w,),
              Align(alignment: Alignment.center,
              child: Text(
                fullname,
                style: GoogleFonts.raleway(
                  textStyle: TextStyle(
                    fontSize: 20.sp,
                    fontWeight: FontWeight.w600,
                    height: 23/20.w,
                    color: Color(0xFF2B2B2B)
                  )
                ),
              ),),
              SizedBox(height: 8.w,),
              Align(alignment: Alignment.center,
                child: Text(
                  AppLocale.changeAvatar.getString(context),
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w600,
                          height: 16/12.w,
                          color: Color(0xFF48B2E7)
                      )
                  ),
                ),),
              SizedBox(height: 20.w,),
              ProfileCustomField(label: 'Имя', controller: firstname, redactor: redactor,),
              SizedBox(height: 16.w,),
              ProfileCustomField(label: 'Фамилия', controller: lastname, redactor: redactor,),
              SizedBox(height: 16.w,),
              ProfileCustomField(label: 'Адрес', controller: address, redactor: redactor,),
              SizedBox(height: 16.w,),
              ProfileCustomField(label: 'Телефон', controller: phone, redactor: redactor,),
              SizedBox(height: 16.w,),


            ],
          ),
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
