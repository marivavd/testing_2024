import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/data/models/category.dart';
import 'package:untitled1/data/models/model_product.dart';
import 'package:untitled1/domain/categories_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/favourite.dart';
import 'package:untitled1/presentation/pages/favourites.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/backButton.dart';

class Details extends StatefulWidget {
  Details({super.key, required this.product});
  ModelProduct product;


  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  bool flag = false;
  ModelCategory? category;
  int currentIndex = 0;
  CatUseCase useCase = CatUseCase();
  bool maxLine = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await useCase.getMyCategoryById(
        widget.product.categoryId,
      (p0){
          category = p0;
          flag = true;
          setState(() {

          });
      }, (error)async{
          showError(context, error);
      }
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 48.w,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtontoBack(),
                  Padding(padding: EdgeInsets.only(top: 12.w),child: Text(
                    'Sneaker Shop',
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16.sp,
                            height: 20/16.w,
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF2B2B2B)
                        )
                    ),
                  ),),
                  InkWell(
                    onTap: (){
                    },
                    child: Stack(
                      children: [

                        Container(
                          height: 44.w,
                          width: 44.w,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(40.w)
                          ),
                          padding: EdgeInsets.all(10.w),
                          child: SvgPicture.asset('assets/bag-2.svg', height: 24.w, width: 24.w, color: Color(0xFF2B2B2B)),
                        ),
                        Padding(padding: EdgeInsets.only(top: 3.w, left: 34.w),child: SvgPicture.asset('assets/Ellipse 886.svg'),),
                      ],
                    )
                  )
                ],
              ),
              SizedBox(height: 26.w,),
              Text(
                widget.product.title,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        fontSize: 26.sp,
                        height: 62/52.w,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFF2B2B2B)
                    )
                ),
              ),
              SizedBox(height: 8.w,),
              Text(
                category!.title,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        fontSize: 16.sp,
                        height: 19/16.w,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF6A6A6A)
                    )
                ),
              ),
              SizedBox(height: 8.w,),
              Text(
                '₽${widget.product.cost.toString()}',
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        fontSize: 24.sp,
                        height: 28/24.w,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFF2B2B2B)
                    )
                ),
              ),
              SizedBox(height: 22.w,),
              Center(
                child: Container(
                  width: 241.w,
                  height: 125.w,
                  child: CachedNetworkImage(
                    imageUrl: widget.product.covers[currentIndex],
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(height: 79.w,),
              SizedBox(
                height: 56.w,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                    itemBuilder: (_, index){
                      return InkWell(
                        onTap: (){
                          setState(() {
                            currentIndex = index;
                          });
                        },
                        child: Container(
                          height: 56.w,
                          width: 56.w,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(16.w)
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal:2.w, vertical: 14.5.w),
                            child: CachedNetworkImage(imageUrl: widget.product.covers[index]),
                          ),
                        ),
                      );
                    }, 
                    separatorBuilder: (_, index){
                      return SizedBox(width: 14.w,);
                    },
                    itemCount: widget.product.covers.length)
              ),
              SizedBox(height: 33.w,),
              Text(
                widget.product.description,
                maxLines: (maxLine)?3:100,
                overflow: TextOverflow.ellipsis,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        fontSize: 14.sp,
                        height: 72/42.w,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF6A6A6A)
                    )
                ),
              ),
              SizedBox(height: 9.w,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: (){
                      setState(() {
                        maxLine = !maxLine;
                      });
                    },
                     child: Text(
                       'Подробнее',
                       style: GoogleFonts.raleway(
                           textStyle: TextStyle(
                               fontSize: 14.sp,
                               height: 21/14.w,
                               fontWeight: FontWeight.w400,
                               color: Color(0xFF48B2E7)
                           )
                       ),
                     ),
                  )
                ],
              ),
              SizedBox(height: 60.w,),
              Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (_) => Favourites()));
                      },
                      child: Container(
                          height: 52.w,
                          width: 52.w,
                          decoration: BoxDecoration(
                              color:Color(0xFFD9D9D9),
                              borderRadius: BorderRadius.circular(40.w)
                          ),
                          child: Align(
                            alignment: Alignment.center,
                            child: SvgPicture.asset('assets/heart_no.svg', width: 24.w, height: 24.w, color: Color(0xFF2B2B2B)),
                          )
                      ),
                    ),
                    SizedBox(
                      height: 52.w,
                      width: 265.w,
                      child: FilledButton(
                        key: Key('SignInButton'),
                        onPressed: ()async{
                        },
                        style: FilledButton.styleFrom(
                            backgroundColor: Color(0xFF48B2E7),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.w)
                            )
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(15.w),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 24.w,
                                  width: 24.w,

                                  child: SvgPicture.asset('assets/bag-2.svg', color: Colors.white),
                                ),
                                Text(
                                  AppLocale.addToCart.getString(context),
                                  textAlign: TextAlign.end,
                                  style: GoogleFonts.raleway(
                                      textStyle: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14.sp,
                                          height: 22/14.w,
                                          fontWeight: FontWeight.w600
                                      )
                                  ),
                                ),
                                Container(width: 24.w, height: 24.w,)
                              ],
                            ),
                          ),
                        )
                      ),
                    )
                  ],
                ),
              )


            ],
          ),
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
