import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/favourite.dart';
import 'package:untitled1/presentation/pages/favourites.dart';
import 'package:untitled1/presentation/widgets/backButton.dart';
import 'package:untitled1/presentation/widgets/cardproduct.dart';

import '../../data/models/model_product.dart';

class Popular extends StatefulWidget {

   Popular({super.key, required this.products});
   List<ModelProduct> products;

  @override
  State<Popular> createState() => _PopularState();
}

class _PopularState extends State<Popular> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 48.w,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ButtontoBack(),
                Text(
                  AppLocale.popular.getString(context),
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontSize: 16.sp,
                          height: 24/16.w,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF2B2B2B)
                      )
                  ),
                ),
        InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (_) => Favourites()));
          },
          child: Container(
            height: 40.w,
            width: 40.w,
            decoration: BoxDecoration(
                color:Colors.white,
                borderRadius: BorderRadius.circular(40.w)
            ),
            child: Align(
              alignment: Alignment.center,
              child: SvgPicture.asset('assets/heart_no.svg', width: 18.w, height: 16.w, color: Color(0xFF2B2B2B)),
            )
          ),
        )
              ],
            ),
            SizedBox(height: 20.w,),

          ],
        ),
      ),
    );
  }
}
