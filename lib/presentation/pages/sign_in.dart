import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/domain/sign_in_use_case.dart';
import 'package:untitled1/presentation/localization.dart';
import 'package:untitled1/presentation/pages/forgot_pass.dart';
import 'package:untitled1/presentation/pages/home.dart';
import 'package:untitled1/presentation/pages/sign_up.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';
import 'package:untitled1/presentation/widgets/textfield.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  var email = TextEditingController();
  var password = TextEditingController();
  SignInUseCase useCase = SignInUseCase();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 121.w,),
              Center(
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    AppLocale.signInTitle.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                        color: Color(0xFF2B2B2B),
                        fontSize: 32.sp,
                        height: 38/32.w,
                        fontWeight: FontWeight.w700
                      )
                    ),
                  ),

                ),
              ),
              SizedBox(height: 8.w,),
              Center(
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    AppLocale.signInText.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF707B81),
                            fontSize: 16.sp,
                            height: 48/32.w,
                            fontWeight: FontWeight.w400
                        )
                    ),
                  ),

                ),
              ),
              SizedBox(height: 30.w,),
              CustomField(label: AppLocale.email.getString(context), hint: 'xyz@gmail.com', controller: email, key: Key('Email')),
              SizedBox(height: 30.w,),
              CustomField(label: AppLocale.password.getString(context), hint: '••••••••', controller: password, enableObscure: true,),
              SizedBox(height: 12.w,),
              InkWell(
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    AppLocale.recovery.getString(context),
                    textAlign: TextAlign.end,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF707B81),
                            fontSize: 12.sp,
                            height: 16/12.w,
                            fontWeight: FontWeight.w400
                        )
                    ),
                  ),
                ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (_) => Forgot()));
                },
              ),
              SizedBox(height: 24.w,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50.w,
                  width: double.infinity,
                  child: FilledButton(
                    key: Key('SignInButton'),
                    onPressed: ()async{
                      showLoading(context);
                      await useCase.pressButton(email.text,
                          password.text,
                              (p0){
                        hideLoading(context);
                        Navigator.push(context, MaterialPageRoute(builder: (_) => Home()));
                              },
                          (error)async{
                        hideLoading(context);
                        await showError(context, error);
                          }
                      );
                    },
                    style: FilledButton.styleFrom(
                      backgroundColor: Color(0xFF48B2E7),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14.w)
                      )
                    ),
                    child: Text(
                      AppLocale.signInButton.getString(context),
                      textAlign: TextAlign.end,
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: Color(0xFFF7F7F9),
                              fontSize: 14.sp,
                              height: 22/14.w,
                              fontWeight: FontWeight.w600
                          )
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 209.w,),
              Center(
                child: InkWell(
                  child: SizedBox(
                    width: double.infinity,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text: AppLocale.areYouFirst.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  color: Color(0xFF6A6A6A),
                                  fontSize: 16.sp,
                                  height: 19/16.w,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                          children: [
                            TextSpan(
                                text: AppLocale.create.getString(context),
                                style: GoogleFonts.raleway(
                                    textStyle: TextStyle(
                                        color: Color(0xFF2B2B2B),
                                        fontSize: 16.sp,
                                        height: 19/16.w,
                                        fontWeight: FontWeight.w500
                                    )
                                ))
                          ]
                      ),
                    ),
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (_) => SignUp()));
                  },
                )
              )


            ],
          ),
        ),
      ),
    );
  }
}
