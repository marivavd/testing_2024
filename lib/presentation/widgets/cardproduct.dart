import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untitled1/data/models/model_product.dart';
import 'package:untitled1/domain/cart_use_case.dart';
import 'package:untitled1/presentation/pages/details.dart';
import 'package:untitled1/presentation/utils/dialogs.dart';

class CardProduct extends StatefulWidget {
  ModelProduct product;
  CardProduct({super.key, required this.product});

  @override
  State<CardProduct> createState() => _CardProductState();
}

class _CardProductState extends State<CardProduct> {
  CartUseCase useCase = CartUseCase();
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 182.w,
      width: 160.w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.w)
      ),
      child: Stack(
        children: [
          Padding(
              padding: EdgeInsets.only(left: 9.w, top: 9.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(padding: EdgeInsets.only(top: 9.w, left: 12.w, right: 21.w),
                child: InkWell(
                  child: SizedBox(
                    width: double.infinity,
                    height: 70.w,
                    child: AspectRatio(
                      aspectRatio: 182/160.w,
                      child: CachedNetworkImage(
                        imageUrl: widget.product.covers.first,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (_) => Details(product: widget.product)));
                  },
                ),),
              SizedBox(height: 12.w,),
              Text(
                'Best Seller',
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        fontSize: 12.sp,
                        height: 16/12.w,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF48B2E7)
                    )
                ),
              ),
              SizedBox(height: 8.w,),
              Text(
                widget.product.title,
                maxLines: 1,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        fontSize: 16.sp,
                        height: 20/16.w,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFF6A6A6A)
                    )
                ),
              ),
              Expanded(child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  children: [
                    Expanded(child: Text('₽${widget.product.cost.toString()}',
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontSize: 14.sp,
                              height: 16/14.w,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF2B2B2B)
                          )
                      ),
                    )),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: GestureDetector(
                        child: Container(
                          height: 34.w,
                          width: 34.w,
                          decoration: BoxDecoration(
                              color: Color(0xFF48B2E7),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(14.w),
                                bottomRight: Radius.circular(14.w),
                              )
                          ),
                          child: InkWell(
                            child: (!widget.product.isBask)?Align(alignment: Alignment.center,
                              child: Text(
                                '+',
                                style: GoogleFonts.raleway(
                                    textStyle: TextStyle(
                                        fontSize: 32.sp,
                                        height: 15.32/32.w,
                                        color: Colors.white
                                    )
                                ),
                              ),):Padding(
                              padding: EdgeInsets.all(11.w),
                              child: SvgPicture.asset('assets/cart.svg'),
                            ),
                            onTap: (){
                              showLoading(context);
                              if (widget.product.isBask){
                                widget.product.count += 1;
                                useCase.updatecountBasket(
                                widget.product.id,
                                    widget.product.count,
                                        (p0){
                                  hideLoading(context);
                                  setState(() {

                                  });
                                        }, (error)async{
                                  hideLoading(context);
                                  showError(context, error);
                                });
                              }
                              else{
                                widget.product.isBask = true;
                                useCase.addBasket(
                                    widget.product.id,
                                        (p0){
                                      hideLoading(context);
                                      setState(() {

                                      });
                                    }, (error)async{
                                  hideLoading(context);
                                  showError(context, error);
                                });
                              }
                            },
                          )
                        ),
                      ),
                    )
                  ],
                ),
              ))

            ],
          ),),
          Padding(padding: EdgeInsets.only(top: 9.w, left: 9.w),child: Container(
            height: 28.w,
            width: 28.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.w),
              color: Color(0xFFF7F7F9)
              
            ),
            child: InkWell(
              child: Align(
                alignment: Alignment.center,
                child: (widget.product.isFavourite)?
                SvgPicture.asset('assets/heart.svg', height: 10.67.w, width: 12.w):
                SvgPicture.asset('assets/heart_no.svg', height: 10.67.w, width: 12.w, color: Color(0xFF2B2B2B))
                ,
              ),

              onTap: (){
                widget.product.isFavourite = !widget.product.isFavourite;
                setState(() {
                });
              },
            )
          ),)
        ],
      ),
    );
  }
}
