
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfileCustomField extends StatefulWidget {
  final String label;
  final TextEditingController controller;
  bool redactor;
  ProfileCustomField({super.key, required this.label, required this.controller, this.redactor = true});

  @override
  State<ProfileCustomField> createState() => _ProfileCustomFieldState();
}

class _ProfileCustomFieldState extends State<ProfileCustomField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: GoogleFonts.raleway(
              textStyle: TextStyle(
                  fontSize: 16.sp,
                  height: 19/16.w,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFF2B2B2B)
              )
          ),
        ),
        SizedBox(height: 17.w,),
        Container(
            height: 48.w,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color(0xFFF7F7F9),
                borderRadius: BorderRadius.circular(14.w)
            ),
            child: TextField(
              controller: widget.controller,
              enabled: widget.redactor,
              style: GoogleFonts.raleway(
                textStyle: TextStyle(
                  fontSize: 14.sp,
                  height: 16/14.w,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF2B2B2B)
                )
              ),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left: 16.w, top: 13.w, bottom: 19.w),

              ),
            )
        )
      ],
    );
  }
}
