
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  const CustomField({super.key, required this.label, required this.hint, required this.controller, this.enableObscure = false});

  @override
  State<CustomField> createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  bool isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: GoogleFonts.raleway(
            textStyle: TextStyle(
              fontSize: 16.sp,
              height: 20/16.w,
              fontWeight: FontWeight.w500,
              color: Color(0xFF2B2B2B)
            )
          ),
        ),
        SizedBox(height: 12.w,),
        Container(
          height: 48.w,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Color(0xFFF7F7F9),
            borderRadius: BorderRadius.circular(14.w)
          ),
          child: TextField(
            controller: widget.controller,
            obscureText: (widget.enableObscure)?isObscure:false,
            obscuringCharacter: '•',
            decoration: InputDecoration(
              hintText: widget.hint,
              hintStyle: TextStyle(
    fontSize: 14.sp,
    height: 16/14.w,
    fontWeight: FontWeight.w500,
    color: Color(0xFF6A6A6A)
    ),

              border: InputBorder.none,
                contentPadding: EdgeInsets.symmetric(vertical: 16.w, horizontal: 14.w),
              suffixIconConstraints: BoxConstraints(minWidth: 20.w),
              suffixIcon: (widget.enableObscure)?GestureDetector(
                onTap: (){
                  setState(() {
                    isObscure = !isObscure;
                  });
                },
                child: (isObscure)?SvgPicture.asset('assets/eye_close.svg', height: 16.w, width: 20.w,
                    color: Color(0xFF6A6A6A)):SvgPicture.asset('assets/eye.svg', color: Color(0xFF6A6A6A)),
              ):null


            ),
          )
        )
      ],
    );
  }
}
