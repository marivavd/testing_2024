import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:untitled1/data/models/model_generate.dart';
import 'package:untitled1/data/repository/keys.dart';

import '../models/model_ai.dart';
import '../models/model_generation.dart';
import '../models/model_style.dart';


final dio = Dio();

Future<void> fetchStyles(
    Function(List<ModelStyle>) onResponse
    ) async {
  Response response = await dio.get(
      "https://cdn.fusionbrain.ai/static/styles/api"
  );
  List<dynamic> data = response.data;
  var result = data.map((e) => ModelStyle.fromJson(e)).toList();
  onResponse(result);
}

Future<void> getModelsAI(
    Function(List<ModelAi>) onResponse,
    Function(String) onError
    ) async {
  Response response = await dio.get(
      "https://api-key.fusionbrain.ai/key/api/v1/models",
      options: Options(
          headers: fetchHeadersTokens()
      )
  );
  List<dynamic> data = response.data;
  var result = data.map((e) => ModelAi.fromJson(e)).toList();
  if (result.isEmpty){
    onError("Модели AI не найдены");
  }
  onResponse(result);
}

Future<void> startGenerate(
    ModelGenerateRequest modelGenerateRequest,
    ModelAi modelAI,
    {
      required Function(String uuid) onInitGenerate
    }
    ) async {
  var jsonParams = jsonEncode(modelGenerateRequest.toJson());
  var formData = FormData.fromMap(
    {
      "model_id": modelAI.id,
      "params": MultipartFile.fromString(
          jsonParams,
          contentType: MediaType("application", "json")
      )
    },
  );

  Response response = await dio.post(
      "https://api-key.fusionbrain.ai/key/api/v1/text2image/run",
      options: Options(
          headers: fetchHeadersTokens()
      ),
      data: formData
  );
  Map<String, dynamic> data = response.data;

  onInitGenerate(data["uuid"]);
}

Future<void> checkGenerate(
    String uuid,
    {
      required Function(ModelGeneration) onDone,
      required Function(String status) onCheckStatus,
      required Function(String) onError
    }
    ) async {
  Response response = await dio.get(
    "https://api-key.fusionbrain.ai/key/api/v1/text2image/status/$uuid",
    options: Options(
        headers: fetchHeadersTokens()
    ),
  );
  var model = ModelGeneration.fromJson(response.data);
  switch (model.status) {
    case "INITIAL":
    case "PROCESSING":
      onCheckStatus(model.status);
    case "FAIL":
      onError("Ошибка генерации");
    case "DONE":
      if (model.images?.isEmpty ?? true){
        onError("Ошибка получения изображения");
      }
      onDone(model);
  }
}