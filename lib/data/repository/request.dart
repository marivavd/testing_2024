import 'dart:typed_data';

import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:untitled1/data/models/category.dart';
import 'package:untitled1/data/models/model_product.dart';
import 'package:untitled1/main.dart';

Future<void> signIn(email, password)async{

  final AuthResponse res = await supabase.auth.signInWithPassword(
    email: email,
    password: password,
  );
}
Future<void> sendCode(email)async{
  await supabase.auth.resetPasswordForEmail(email);
}
Future<void> signUp(email, password, name)async{

  final AuthResponse res = await supabase.auth.signUp(
    email: email,
    password: password
  );
  await supabase.from('profiles').insert({'user_id': res.user!.id, 'firstname': name, 'address': '', 'lastname': '', 'photo': '', 'phone': ''});
}

Future<Map<String, dynamic>> getProfileAttributes()async{
  return await supabase.from('profiles').select().eq('user_id', supabase.auth.currentUser!.id).single();
}

Future<dynamic> getAvatar()async{
  var profile = await supabase.from('profiles').select().eq('user_id', supabase.auth.currentUser!.id).single();
  if (profile['photo'] == '' || profile['photo'] == null){
    return null;
  }
  else{
    return await supabase.storage.from('avatars').download(profile['photo']);
  }
}
Future<void> verifyOtp(email, code)async{
  final AuthResponse res = await supabase.auth.verifyOTP(
    type: OtpType.email,
    token: code,
    email: email,
  );
}
Future<void> editProfile(String firstname, String lastname, String phone, String address)async{
  await supabase.from('profiles').update({'firstname': firstname, 'lastname': lastname, 'phone': phone,
  'address': address}).eq('user_id', supabase.auth.currentUser!.id);
}
Future<void> uploadAvatar(Uint8List bytes) async {
  var name = "${supabase.auth.currentUser!.id}.png";
  if (supabase.from('profiles').select('photo').eq('user_id', supabase.auth.currentUser!.id).single() != ''){
    await supabase.storage
        .from("avatars")
        .remove(["${supabase.auth.currentUser!.id}.png"]);
  }

  await supabase.storage
      .from("avatars")
      .uploadBinary(
      name,
      bytes
  );
  await supabase.from('profiles').update({'photo': name}).eq('user_id', supabase.auth.currentUser!.id);
}
Future<void> removeAvatar() async {
  await supabase.storage
      .from("avatars")
      .remove(["${supabase.auth.currentUser!.id}.png"]);
  await supabase.from('profiles').update({'photo': ''}).eq('user_id', supabase.auth.currentUser!.id);
}


Future<void> changePass(password)async{
  final UserResponse res = await supabase.auth.updateUser(
    UserAttributes(
      password: password
    ),
  );
}

Future<List<ModelCategory>> geCategories()async{
  var response = await supabase.from('categories').select();
  List<ModelCategory> result = [];
  for (var val in response){
    result.add(ModelCategory(id: val['id'], title: val['title']));
  }
  return result;
}

Future<List<ModelProduct>> getBestProducts()async{
  var response = await supabase.from('products').select().eq('is_best_seller', true);
  List<ModelProduct> result = [];
  for (var val in response){
    List<String> covers = await getCovers(val['id']);
    result.add(ModelProduct(id: val['id'], title: val['title'], description: val['description'],
        best: val['is_best_seller'], cost: double.parse(val['cost'].toString()), categoryId: val['category_id'], covers: covers, isFavourite: await inFavourite(val['id']),
    isBask: await inBasket(val['id']), count: await countBasket(val['id'])));
  }
  print(result);
  return result;
}

Future<List<ModelProduct>> getAllProducts()async{
  var response = await supabase.from('products').select();
  List<ModelProduct> result = [];
  for (var val in response){
    List<String> covers = await getCovers(val['id']);
    result.add(ModelProduct(id: val['id'], title: val['title'], description: val['description'],
        best: val['is_best_seller'], cost: double.parse(val['cost'].toString()), categoryId: val['category_id'], covers: covers, isFavourite: await inFavourite(val['id']),
        isBask: await inBasket(val['id']), count: await countBasket(val['id'])));
  }
  print(result);
  return result;
}

Future<List<String>> getCovers(String id)async{
  final List<FileObject> objects = await supabase
      .storage
      .from('images')
      .list();
  List<String> result = [];
  for (var val in objects){
    if (val.name.contains(id)){
      result.add(supabase
          .storage
          .from('images')
          .getPublicUrl(val.name));
    }
  }
  return result;
}


Future<ModelCategory> geCategoryById(String id)async{
  var response = await supabase.from('categories').select().eq('id', id);
  List<ModelCategory> result = [];
  for (var val in response){
    result.add(ModelCategory(id: val['id'], title: val['title']));
  }
  return result.first;
}

Future<bool> inBasket(String id)async{
  var response = await supabase.from('cart').select().eq('product_id', id);
  if (response.length == 0){
    return false;
  }
  return true;
}
Future<void> addToBasket(String id)async{
  var response = await supabase.from('cart').insert({'product_id': id, 'user_id': supabase.auth.currentUser!.id, 'count': 1});
}
Future<void> removeToBasket(String id)async{
  var response = await supabase.from('cart').delete().eq('product_id', id).eq('user_id', supabase.auth.currentUser!.id);
}
Future<void> updateToBasket(String id, int count)async{
  var response = await supabase.from('cart').update({'count': count}).eq('product_id', id).eq('user_id', supabase.auth.currentUser!.id);
}
Future<bool> inFavourite(String id)async{
  var response = await supabase.from('favourite').select().eq('product_id', id);
  if (response.length == 0){
    return false;
  }
  return true;
}

Future<int> countBasket(String id)async{
  var response = await supabase.from('cart').select().eq('product_id', id);
  if (response.length == 0){
    return 1;
  }
  return response.first['count'];
}
