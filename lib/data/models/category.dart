class ModelCategory{
  final String id;
  final String title;

  ModelCategory({required this.id, required this.title});

}