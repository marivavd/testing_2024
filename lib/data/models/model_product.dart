class ModelProduct{
  final String id;
  final String title;
  final String description;
  final bool best;
  final double cost;
  final String categoryId;
  final List<dynamic> covers;
  bool isFavourite;
   bool isBask;
  int count;

  ModelProduct({required this.id, required this.title, required this.description,
    required this.best, required this.cost, required this.categoryId, required this.covers,
  this.isBask = false,
  this.isFavourite = false,
  this.count = 1});
}