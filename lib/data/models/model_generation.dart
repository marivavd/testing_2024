/// YApi QuickType插件生成，具体参考文档:https://plugins.jetbrains.com/plugin/18847-yapi-quicktype/documentation

import 'dart:convert';

ModelGeneration modelGenerationFromJson(String str) => ModelGeneration.fromJson(json.decode(str));

String modelGenerationToJson(ModelGeneration data) => json.encode(data.toJson());

class ModelGeneration {
  ModelGeneration({
    required this.images,
    required this.generationTime,
    required this.uuid,
    required this.censored,
    required this.status,
  });

  List<String> images;
  int generationTime;
  String uuid;
  bool censored;
  String status;

  factory ModelGeneration.fromJson(Map<String?, dynamic> json) => ModelGeneration(
    images: List<String>.from(json["images"]?.map((x) => x)??[]),
    generationTime: json["generationTime"]??-1,
    uuid: json["uuid"]??'',
    censored: json["censored"]??false,
    status: json["status"]??'',
  );

  Map<dynamic, dynamic> toJson() => {
    "images": List<dynamic>.from(images.map((x) => x)),
    "generationTime": generationTime,
    "uuid": uuid,
    "censored": censored,
    "status": status,
  };
}
