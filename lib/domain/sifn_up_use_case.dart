import 'package:untitled1/data/repository/request.dart';
import 'package:untitled1/domain/check.dart';
import 'package:untitled1/domain/utils.dart';

class SignUpUseCase{
  Future<void> pressButton(email, password, name, Function(void) onResponse, Future<void> Function(String) onError)async {
    Check check = Check();
    if (check.checkEmail(email) == 0){
      print(email);
      onError('Email incorrect');

    }
    else if (check.checkPassword(password) == 0){
      onError('Pass incorrect');

    }
    else{
      requestSignIn() async{
        await signUp(email, password, name);
      }
      requests(requestSignIn, onResponse, onError);
    }
  }
}