import 'package:dio/dio.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> requests<T>(
Future<T> Function() request,
Function(T) onResponse,
Future<void> Function(String) onError
)async{
  try{
    var response = await request();
    onResponse(response);
  }
  on AuthApiException catch(e){
    onError(e.message);
  }
  catch(e){
    onError(e.toString());
  }
}

Future<bool> request(
    Future<void> Function() func,
    Function(String) onError
    ) async {
  try{
    await func();
    return true;
  } on DioException catch (e){
    try{
      if (e.response?.data != null){
        onError('Не найдено');
      }
      return false;
    }catch(_){
      var code = e.response?.statusCode;
      if (e.response != null) {
        if (e.response!.statusCode != null){
          onError('Не найдено');
          return false;
        }else{
          onError(
              "STATUS: $code\n"
                  "MESSAGE: ${e.response?.statusMessage}"
          );
          return false;
        }
      } else {
        onError("Ошибка при отправке запроса");
        return false;
      }
    }
  }catch (e) {
    onError(e.toString());
    return false;
  }
}

