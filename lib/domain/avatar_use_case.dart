import 'dart:typed_data';

import 'package:image_picker/image_picker.dart';

class AvatarUseCase{
  XFile? avatar;
  final Future<ImageSource?> Function() chooseImageSource;
  final Function(Uint8List) onPickAvatar;
  final Function() onRemoveAvatar;

  AvatarUseCase({required this.chooseImageSource, required this.onPickAvatar, required this.onRemoveAvatar});


  void pressButton(bool flag){
    if (flag){
      pressRemoveButton();

    }
    else{
      pressChangeButton();

    }
  }
  Future<void> pressChangeButton()async{
    var source = await chooseImageSource();
    if (source == null){
      return;
    }
    avatar = await ImagePicker().pickImage(source: source);
    var bytes = await avatar?.readAsBytes();
    if (bytes != null){
      onPickAvatar(bytes);
    }

  }

  Future<void> pressRemoveButton()async {
    avatar = null;
    onRemoveAvatar();
  }
}
