import 'package:untitled1/data/repository/request.dart';
import 'package:untitled1/domain/check.dart';
import 'package:untitled1/domain/utils.dart';

class ForgotUseCase{
  Future<void> pressButton(email, Function(void) onResponse, Future<void> Function(String) onError)async {
    Check check = Check();
    if (check.checkEmail(email) == 0){
      print(email);
      onError('Email incorrect');

    }
    else{
      requestSignIn() async{
        await sendCode(email);
      }
      requests(requestSignIn, onResponse, onError);
    }
  }
}