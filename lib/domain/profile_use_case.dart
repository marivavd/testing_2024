import 'package:untitled1/data/repository/request.dart';
import 'package:untitled1/domain/utils.dart';

class  ProfileUseCase{
  Future<void> getAttributes(Function(dynamic) onResponse, Future<void> Function(String) onError)async {
      requestSignIn() async{
        return await getProfileAttributes();
      }
      requests(requestSignIn, onResponse, onError);
  }

  Future<void> getProfileAvatar(Function(dynamic) onResponse, Future<void> Function(String) onError)async {
    requestSignIn() async{
      return await getAvatar();
    }
    requests(requestSignIn, onResponse, onError);
  }

  Future<void> edit(String firstname, String lastname, String phone, String address, Function(dynamic) onResponse, Future<void> Function(String) onError)async {
    requestSignIn() async{
      return await editProfile(firstname, lastname, phone, address);
    }
    requests(requestSignIn, onResponse, onError);
  }

  Future<void> loadAvatar(avatar, Function(dynamic) onResponse, Future<void> Function(String) onError)async {
    if(avatar == null){
      requestSignIn() async{
        return await removeAvatar();
      }
      requests(requestSignIn, onResponse, onError);
    }
    else{
      requestSignIn() async{
        return await uploadAvatar(avatar);
      }
      requests(requestSignIn, onResponse, onError);
    }

  }
}