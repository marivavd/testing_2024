import 'package:untitled1/data/repository/request.dart';
import 'package:untitled1/domain/check.dart';
import 'package:untitled1/domain/utils.dart';

class SignInUseCase{
  Future<void> pressButton(email, password, Function(void) onResponse, Future<void> Function(String) onError)async {
    Check check = Check();
    if (check.checkEmail(email) == 0){
      print(email);
      onError('Email incorrect');

    }
    else if (check.checkPassword(password) == 0){
      onError('Pass incorrect');

    }
    else{
      requestSignIn() async{
        await signIn(email, password);
      }
      requests(requestSignIn, onResponse, onError);
    }
  }
}