import 'package:untitled1/data/repository/request.dart';
import 'package:untitled1/domain/check.dart';
import 'package:untitled1/domain/utils.dart';

class CatUseCase{
  Future<void> getAllCategories( Function(dynamic) onResponse, Future<void> Function(String) onError)async {

      requestSignIn() async{
        return await geCategories();
      }
      requests(requestSignIn, onResponse, onError);

  }
  Future<void> getMyCategoryById( String id, Function(dynamic) onResponse, Future<void> Function(String) onError)async {

    requestSignIn() async{
      return await geCategoryById(id);
    }
    requests(requestSignIn, onResponse, onError);

  }

}