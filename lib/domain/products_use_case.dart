import 'package:untitled1/data/repository/request.dart';
import 'package:untitled1/domain/check.dart';
import 'package:untitled1/domain/utils.dart';

class ProductUseCase{
  Future<void> getAllBestSellerProducts( Function(dynamic) onResponse, Future<void> Function(String) onError)async {

    requestSignIn() async{
      return await getBestProducts();
    }
    requests(requestSignIn, onResponse, onError);

  }

  Future<void> getWholeProducts( Function(dynamic) onResponse, Future<void> Function(String) onError)async {

    requestSignIn() async{
      return await getAllProducts();
    }
    requests(requestSignIn, onResponse, onError);

  }
}