import 'package:untitled1/data/repository/request.dart';
import 'package:untitled1/domain/check.dart';
import 'package:untitled1/domain/utils.dart';

class CartUseCase{
  Future<void> addBasket( String id, Function(dynamic) onResponse, Future<void> Function(String) onError)async {

    requestSignIn() async{
      return await addToBasket(id);
    }
    requests(requestSignIn, onResponse, onError);

  }

  Future<void> updatecountBasket( String id, int count, Function(dynamic) onResponse, Future<void> Function(String) onError)async {

    requestSignIn() async{
      return await updateToBasket(id, count);
    }
    requests(requestSignIn, onResponse, onError);

  }
  Future<void> removeBasket( String id, Function(dynamic) onResponse, Future<void> Function(String) onError)async {

    requestSignIn() async{
      return await removeToBasket(id);
    }
    requests(requestSignIn, onResponse, onError);

  }

}